﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
	class Program
	{
		static void Main(string[] args)
		{
			if (int.TryParse("111", out var result))
				Console.WriteLine(result);
			else
				Console.WriteLine("Could not parse input");
		}
	}
}
